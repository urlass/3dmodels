from solid import *
d = difference()(
    cube(15, center=True),
    sphere(10)
)
scad_render_to_file(d, 'scratch.scad')
