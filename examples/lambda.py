from math import sin

from solid import *
from solid.utils import arc

# main straight line
x = 84;
y = 10
z = 160
main_line = rotate([45, 0, 0])( cube(size = [x,y,z], center = True) ) 

#tails -- need to redo this, can't render them because 2d & 3d don't mix
arc_radius = 30
arc_width = 10

top_start_degrees = 45
top_end_degrees = 180

top_tail = rotate([90, 0, 90]) (arc(rad=arc_radius, start_degrees=top_start_degrees,
                                    end_degrees=top_end_degrees))
top_tail -= rotate([90, 0, 90]) (arc(rad=arc_radius-arc_width, start_degrees=top_start_degrees,
                                 end_degrees=top_end_degrees))
top_tail = translate([0, -1*z*sin(45)/2, z*sin(45)/2 - (arc_radius+arc_width/2)]) (top_tail)

bottom_start_degrees = 225
bottom_end_degrees = 0
bottom_tail = rotate([90, 0, 90]) (arc(rad=arc_radius, start_degrees=bottom_start_degrees,
                                       end_degrees=bottom_end_degrees))
bottom_tail -= rotate([90, 0, 90]) (arc(rad=arc_radius - arc_width, start_degrees=bottom_start_degrees,
                                        end_degrees=bottom_end_degrees))
bottom_tail = translate([0, z*sin(45)/2, -1*z*sin(45)/2 + (arc_radius+arc_width/2)]) (bottom_tail)

# supporting straight line
scale_factor = 0.75
x *= scale_factor
y *= scale_factor
z *= scale_factor
supporting_line = rotate([-35, 0, 0])(cube(size = [x,y,z], center = True)) 
supporting_line = translate([0, -1*z*sin(45)/2*scale_factor, -1*z*sin(45)/2*scale_factor])\
        (supporting_line)

model = main_line
model += scale([100, 1, 1]) (top_tail)
model += scale([100, 1, 1]) (bottom_tail)
model += supporting_line
model += scale([0.5, 0.5, 0.5]) (model)
scad_render_to_file(model, 'scratch.scad')
